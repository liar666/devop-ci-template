# Add lint, static analysis and compute code coverage

During development, the fact that your code actually compiles and runs as you expect is not sufficient, especially if you work within a team. **Code quality** is also a very important measure, that as a lot of advantages:

- It helps your code to be more readable, makes code review easier, thus the code is less prone to errors and easier to maintain,
- It ensures that everyone in the team uses the same conventions, thus prevents constant small modifications of the code depending on who's formatting it (e.g. if one developer uses spaces and another tabs for indentation,  `git` might list a lot of conflicting lines, whereas the code logic might be the same).

In this Lab, you'll add a few tools to your GitLab CI pipeline, in order to ensure a minimal code quality.

You'll add a **linter**, a **static code analysis** tool and compute **code coverage** statistics.

## Lint

A `linter` is a tool that analyzes your code and renders a report detailing the "errors" in your code (the "errors" are not oriented towards the code soundness, but mostly towards the **beauty of the code presentation**).

For example, the following lines show the output of PyLint on a piece code:

```
No config file found, using default configuration
************* Module src.calculator_test
C:  1, 0: Missing module docstring (missing-docstring)
W:  2, 0: Relative import 'calculator', should be 'src.calculator' (relative-import)
C:  4, 0: Missing class docstring (missing-docstring)
C:  5, 4: Missing method docstring (missing-docstring)
************* Module src.calculator
C:  1, 0: Missing module docstring (missing-docstring)
C:  1, 0: Missing class docstring (missing-docstring)
C:  1, 0: Old-style class defined. (old-style-class)
W:  1, 0: Class has no __init__ method (no-init)
C:  3, 4: Argument name "x" doesn't conform to snake_case naming style (invalid-name)
C:  3, 4: Argument name "y" doesn't conform to snake_case naming style (invalid-name)
C:  3, 4: Missing method docstring (missing-docstring)
C:  6, 4: Argument name "x" doesn't conform to snake_case naming style (invalid-name)
C:  6, 4: Argument name "y" doesn't conform to snake_case naming style (invalid-name)
C:  6, 4: Missing method docstring (missing-docstring)
C:  9, 4: Argument name "x" doesn't conform to snake_case naming style (invalid-name)
C:  9, 4: Argument name "y" doesn't conform to snake_case naming style (invalid-name)
C:  9, 4: Missing method docstring (missing-docstring)

------------------------------------
Your code has been rated at -0.62/10
```

By default, a linter runs a variety of rules, sometimes very hard to always strictly follow by the developers (for instance, `x` and `y` might be sufficient names in the above example).

To fit your team needs, you can use a specific configuration of the linter, where only a few specific set of rules have been enabled.

This is the team's responsibility to discuss which rules to include or not in a given project?

## Static analysis

A `static analysis tool`, is a tool that analyzes your code and reports bad practices (like executing multiple operations on the same line, like `if (i++==--j)` in `C`). Some static analysis tools also perform a check for known security vulnerabilities.

Contrarily to a linter, a `static analysis tool` is thus oriented towards the **code soundness**, not the beauty of its presentation.

However, as both tools have a similar objective (force developers to write better code), the linter is sometimes considered to be part of the static analysis toolchain. Also, in practice, the difference might be difficult to perceive: some rules might be common to both tools (for instance, executing multiple operations on the same line both makes the code difficult to read and is highly probable to create a security vulnerability).

## Code coverage

Finally, `code coverage` is a tool that computes **statistics about the lines of code that are covered by your unit tests, linter & static code analysis tools**.

## TODO

- Choose a linter tool for python
- Enable the linter in the gitlab CI pipeline
- (Optional) Configure the linter to enable/disable some rules_
- Choose a static analysis tool for python
- Enable the static analysis tool in the GitLab CI pipeline
-_(Optional) Configure the static analysis to enable/disable some rules_
- Choose a coverage tool for python
- Enable the coverage tool in the GitLab CI pipeline
- Add badges to your project to display its coverage status on the front page (project description or `README.md` file)

## Resources

- [Introduction to coding conventions](https://en.wikipedia.org/wiki/Coding_conventions)
- [PEP 8 -- Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008/)
- [Introduction to linter](https://en.wikipedia.org/wiki/Lint_(software))
- [PyLint](https://www.pylint.org/)
- [dlint](https://github.com/duo-labs/dlint)
- [Static analysis tools](https://en.wikipedia.org/wiki/List_of_tools_for_static_code_analysis)
- [prospector](https://pypi.org/project/prospector/)
- [Python Code Quality tools](https://github.com/PyCQA)
- [flake8 (includes other tools listed here like PyFlakes, pycodestyle or mccabe)](https://github.com/PyCQA/flake8)
- [pycodestyle](https://github.com/PyCQA/pycodestyle)
- [pyflakes](https://github.com/PyCQA/pyflakes)
- [bandit](https://github.com/PyCQA/bandit)
- [mccabe](https://github.com/PyCQA/mccabe)
- [SonarQube (not specific to Python)](https://en.wikipedia.org/wiki/SonarQube)
- [Introduction to Coverage](https://en.wikipedia.org/wiki/Code_coverage)
- [Introduction to Coverage (in `C`/`C++`)](https://www.bullseye.com/coverage.html)
- [coverage.py](https://coverage.readthedocs.io/en/coverage-5.0.3/)
- [test library (Unit Test + Coverage report)](https://docs.python.org/3/library/test.html)
- [PyTest-cov](https://pypi.org/project/pytest-cov/)
- [More stats about your code](https://github.com/rubik/radon)
- [Security Vulnerability tester](https://github.com/python-security/pyt)
- [Dead code finder](https://github.com/jendrikseipp/vulture)
- [GitLab Badges 1](https://dev.to/ranb2002/what-are-gitlab-badges-3760)
- [GitLab Badges 2](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/41174)
- [AnyBadge](https://github.com/elbosso/anybadge)
- [Shields.io](https://shields.io/)
- [😎 Awesome Badges](https://github.com/badges/awesome-badges)
