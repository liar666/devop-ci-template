# Defense

Today each group will present its final solution.

The final note will include:
- DevOps project (source code: 50% + GitLab pipeline: 50%): 70%
- Presentation: 30%

As you are supposed to work in groups, we will try as much as possible to give a single note to the group. **However, if we notice an imbalance in the work done by each member of the group, we might differentiate the notes to reflect the actual implication of each member.**

[//]: <> (Proposition de PYF: 1. on met une note temporaire de groupe / 2. on demande aux groupes de nous faire une propal de re-répartition des points 3. les profs décident de la note finale en fonction des != infos)

[//]: <> (Bonsoir
Les notes des soutenances de ce matin viennent d'être posées sur Mootse.
Ces notes sont le résultat d'une grille de notation détaillée et éprouvée nous permettant d'obtenir une note objective et rationnelle en rapport avec nos attentes pédagogiques dans ce projet et bien sur la qualité de votre rendu et de votre prestation.
Les notes qui vous sont communiquées seront provisoires et par groupe.
Vous avez jusqu'à lundi midi pour nous faire une éventuelle proposition de répartition adaptée en fonction de la réalité des implications individuelles dans le projet.
Par exemple, le groupe est composé de 4 membres : A, B, C, D.
Il reçoit la note de 15 pour le groupe.
Ceci donne donc 60 points: nb_participants x note_du_groupe.
Le groupe peut soumettre que B obtiendra 18 et les autres membres 14. La somme faisant toujours 60.
Nous vous laisserons donc nous faire une proposition de répartition adaptée en fonction de ce que vous jugerez pertinent - ou bien sur de confirmer la note de groupe.
J'insiste toutefois sur le fait que cette proposition reste une proposition : dans la plupart des cas, nous décidons de suivre vos propositions mais nous restons seuls juges de la note individuelle de chaque étudiant. [GM: notamment si les élèves ne sont pas OK entre eux et qu'on ne les a pas prévenus à l'avance, on doit laisser la note de groupe...]
J'en profite pour vous féliciter de la qualité globale de vos travaux et j'espère que cette exercice très proche de la réalité vous aura apporté de nouvelles compétences.
Nous avons pris notes de vos remarques sur les difficultés rencontrées et les améliorations suggérées. Nous en tiendrons compte dans le cadre de l'évolution continue de ce module.
J'en profite pour vous remercier de l'approche critique constructive que la plupart d'entre vous ont eu.
Je vous souhaite à toutes et à tous un très bon week-end.)

See sections below for more details on what will be expected for each of these elements. 

## Presentation

Each presentation will take 15 minutes and must include the presentation of:

1. the team and its organization
2. a "big picture" of the architecture of your solution [//]: <> (source code organization: files, classes, etc.)
3. [Dev part] the Solution: source code + **a live demo of the product** (ideally we run `docker pull <your image>` from our machines)
4. [Ops part] the Pipeline [//]: <> (stages) and your GitLab workflow (directly on Gitlab)
5. an astonishment report (what you dis/liked about the project and its organization)
[//]: # (- Ops part  # CC: qu'entend-on par GitLab Workflow ?)

For items **1/2/5**, you can create simple presentation slides (1 slide per item) to support your talk. For items 3/4, you can directly show us the code/pipeline in your GitLab account to illustrate your talk.

**For the defense day, please add the following new machine to your list of monitored machines:**
- **For teams 1 to 5, add machine:  `defense1.hopto.org`**
- **For teams 6 to 10, add machine: `defense2.hopto.org`**

## Project

### Description

- Put yourself into the shoes of a user of your software: what info would she like to see on the project page?
  + https://dev.to/scottydocs/how-to-write-a-kickass-readme-5af9

### Code

- As for any project Code **must** be documented
- Code must respect the format defined in *lint* phase of the project
- Code must have maximum *coverage*
- Remember to manage correctly error cases
  - ex: monitored machine is down at startup or shuts down during monitoring, etc.

### Expected functionalities

- Display of a Global Dashboard with all machines
- Display of a Dashboard for each machine
- Display of fixed metrics
  - e.g. cpu model, machine name...
- Display of metrics that evolves with time
  - e.g. graph of the RAM used dugin last hour
- Display of logs metrics
  - parsing, alerts, etc.
- Error management
  - a machine does not respond, etc.
- Easiness of adding a new machine
  - for the sake of simplification, let's consider all machine are the same
  - added machine is similar to MonitorMe*: runs Linux + Apache

[//]: # (- Access to the logs of the machine  # GM: je ne sais même plus ce qu'on voulais dire avec ça ?)
[//]: # (- Display of instantaneous metrics e.g. amount of RAM in use now #  GM: je pense que c'est inclus dans le point suivant. l'année derniere on avait peur que la GUI soit trop dure, alors on avait ajouté ce point pour simplifier mais ce n'est plus pertinent)


## GitLab pipeline

- Is the final version pushed on master?
- Have you created & used correctly the recommended branches (GithubFlow)?
- Are the required steps of pipeline in place (tests, lint, coverage, delivery/deploy)?
- Did you create a working Docker image/Do you use it in your pipeline?
- [Have you pushed your code into a Docker image? Can we simply run your code with a simple command like `docker run/pull <your_image>`?]
- Have you used the merge requests?
- Have you used Kanban / issues in your workflow?
