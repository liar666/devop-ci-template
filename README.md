# DevOps project
## Introduction

Until now, during your courses at TSÉ, you've learned how to develop software (*Software Engineering*) and how to program in several languages (particularly in *C/C++/Python/Java*).

You also had a glimpse at how to test software with JUnit (during the *Software Engineering* and *Java* courses) and packaging with Maven (during the *Java* course).

However, as you've seen in the "Life Cycle" section of the *Software Engineering* course, in "real life", there's more to Software Development than just programming.

We've seen that Software Engineering = $`\int_{time, scale}^{}`$ Programming.

Indeed, Software Development follows a life cycle that goes from the *Ideation* step to the *Maintenance* step and loops back from there in iterative cycles. In case you don't remember, the various intermediary steps are:

1. Ideation & Feasibility
2. Analysis
3. Design
4. Programming/Refactoring
5. Unit Testing
6. Integration Testing
7. Validation Testing
8. Delivery, Deployment & Distribution
9. Maintenance

Of course, as everybody knows, [developers are lazy](https://www.forbes.com/sites/forbestechcouncil/2018/01/08/developers-are-lazy-and-thats-usually-a-good-thing). As a consequence, along the years, they developed tools and methodologies to ease both: each of these steps, and the whole pipeline/workflow automation.

The main result of these improvements is called **DevOps**, as it mixes approaches from both **Dev**elopment (coding and testing) and **Op**eration**s** (installing & maintaining remote servers and daemons, deployment). It focuses on steps 4 to 9.

DevOps is the topic of the present course.

There's no definitively/commonly accepted definition of "DevOps", but it is rather a mindset than an actual Job Title. One simple definition that fits this cours is "DevOps is a set of practices intended to reduce the time between committing a change to a system and the change being placed into normal production, while ensuring high quality".

You're advised to read more [here](https://en.wikipedia.org/wiki/DevOps) and [here](https://youtu.be/M6F6GWcGxLQ) to learn about the context in which this course takes place.

The main steps of DevOps are the following:

1. **Coding**: code development and review (versioning)
2. **Building**: continuous integration tools
3. **Testing**: continuous testing tools
4. **Packaging**: artifact repository
5. **Releasing**: release automation (continuous delivery/deployment)
6. **Configuring**: configuration, infrastructure as code tools
7. **Monitoring**: performance monitoring, end-user experience

Of course, to practice DevOps effectively, software applications have to meet a set of architecturally significant requirements (ASRs), such as: modifiability, testability, deployability, and monitorability. If you cannot access & modify the source code of the application, if you cannot test the application, if you cannot (for legal or technical reasons) deploy the application or if you cannot collect data on if/how it is performing in production, you will not be able to fully apply DevOps.

You're advised to read more about those terms that you might not have encountered/understood in previous lectures, like:

- [**Continuous Integration (CI)**](https://en.wikipedia.org/wiki/Continuous_integration)
- [**French Podcast to understand Continuous Integration bases**](https://youtu.be/JnKueynwwkY)
- [**Artifact**](https://en.wikipedia.org/wiki/Artifact_(software_development))
- [**Packaging**](https://en.wikipedia.org/wiki/Java_package) (remember, you've built [.jar](https://en.wikipedia.org/wiki/JAR_(file_format)) files with [Maven](https://en.wikipedia.org/wiki/Apache_Maven) during your Java projects!)
- [**Software Repository**](https://en.wikipedia.org/wiki/Software_repository) (remember, you've used the [Maven Repository](https://mvnrepository.com/repos/central) during your Java projects to download/update/manage external libraries, like OpenCV!)
- [**Continuous Deployment (CD)**](https://en.wikipedia.org/wiki/Continuous_deployment)
- [**Continuous Delivery (CD)**](https://en.wikipedia.org/wiki/Continuous_delivery)
- [**Infrastructure as Code (IaC)**](https://en.wikipedia.org/wiki/Infrastructure_as_code)
- [**Monitoring (system)**](https://en.wikipedia.org/wiki/System_monitor)
- [**Monitoring (APM)**](https://en.wikipedia.org/wiki/Application_performance_management)

[comment]: # (removing self promotion **French Podcast to understand Monitoring** https://lydra.fr/rdo-6-cest-quoi-la-supervision-dune-infrastructure-cloud/)

## This project

The present project will be an hands-on introduction to the DevOps domain, in which you'll dicover its various aspects by practicising them.

We've decided to make the various Labs integrate into a **global project**, that will act as a common thread to link the various topics we will address in this course and that will allow you to end up the course with a complete solution, that you'll be able to present to us.

You'll work by teams of 4/5 students (randomly selected).

In order to stay in the DevOps domain, we've decided that the topic of the project itself will be to from the DevOps domain: build a Server Monitoring tool (cf. next section for details).

The development of such a monitoring tool will allow you to practice *system programming* (in Python) and *server administration* (ssh connexion, parsing logs...), which are competences you've learned in other courses of this teaching block and are mandatory for DevOps Engineers. As well, while developing your code you'll learn (by practicising) the tools & methodologies to automate the process of programming/refactoring, testing, delivery, deploying and maintaining code, which are also mandatory competences for any DevOps Engineers.

Thus, you'll practice the DevOps methodology while building a DevOps tool. That's neat!

The course will be divided into a suite of ~14 Labs (~3 hrs each => 42 hrs total), so that you can see the many aspects & tools involved in the DevOps process, one at a time.

### Evaluation

The *evaluation* will take place during the last Lab.

- Each team will *pitch* & *demo* its production (30% of the final score).
- We will also review the teams' *source code* and *workflow* (70% of the final score).

## Organization (specific 2020)

As you already know, this year is quite peculiar, with the sanitary crisis. We will have to make most (if not all) the sessions remote.

- As the main entry point, you can join us on the School's Mattermost:
  https://chat.telecomste.fr/2021fise2/channels/projet-interface-admin 
  There, we can discuss in text form. This is useful when we want to share informations with everybody while leaving traces of our conversations.
- But what if we want to discuss more rapidly & informally in groups?
  There's a new functionality in WebEx as of 05 November 2020, that allows meeting organizers (us, teachers) to create sub/split/group-meetings.
  This will be useful during sessions. Unfortunately, this requires the main meeting to remain open for the sub/groups to exist, so this cannot be used between sessions, as we (teachers) will need to use our main channel for our other courses.
- You'll need to find a way to communicate within you group outside the sessions. You're free to choose the tool you prefer, but you can use Private Channels (we can create them if you can't) or Group Talks in Mattermost for text discussions. For audio/video, there are tools like [Jitsi1](http://jitsi.org) [Jitsi2](http://meet.alolise.org) or MS Team (accessible via your subscription to Office 365). A mor ecomplete list of FLOSS collaboration software can be found here: [Service des CHATONS](https://entraide.chatons.org/en/)

## Monitoring Application

As Monitoring is an important part of DevOps, there already exists several turnkey[^turnkey] tools/tool-suites in the "market".

Two examples are [Grafana](https://grafana.com/) or [Beats + ELK Stack](https://www.elastic.co/what-is/elk-stack).

Of course, we will not use these tools, but develop our own (simplified) version. These tools will serve as examples for what your final result might look like.

For instance, below are two examples of monitoring dashboards, one in Grafana, the other with Beats+ELK-Stack:

| Example of Grafana Display | Example of Beats+ELK-Stack Dashboard |
|----------------------------|--------------------------------------|
| <img alt="Example of Grafana Display" src="./imgs/grafana.png" width="95%"/> | <img alt="Example of Beats+ELK-Stack Dashboard" src="./imgs/elk_monitoring.jpg" width="95%"/> |

The general functioning of a Monitoring platform is the following:

| General Overview of a Monitoring solution | Example of the Beats+ELK-Stack monitoring solution |
|----------------------------|--------------------------------------|
| <img alt="General overview of a monitoring solution" src="./imgs/unwanted_architecture.png" width="95%"/> | <img alt="Example of the Beats+ELK-Stack Monitoring solution" src="./imgs/architecture_simplified.png" width="95%"/> |

1. An "agent" (small piece of software that runs in the background) is installed on each machine to be monitored. This agent collects data about the server itself (general performance: CPU/RAM/Network/HardDrive/..., or security: logs/...) and/or a specific application, like a database or a web server (logs/...)
    - For instance in the ELK-Stack example, *Beats* is an agent that collects CPU/RAM/... data and *Logstash* is an agent that parses log files.
2. The agent sends the collected data to a central database
    - In the case of ELK-Stack, the Database is *ElasticSearch*
3. A graphical front-end extracts data from the database and synthetizes the info into multiple graphs, organized in dashboards, that provide an integrated view of the health of the monitored machine(s). It can also send alerts to the administrator of the machine if it detects an abnormal behaviour.
    - In the case of the ELK-Stack example, *Kibana* is the Web GUI

The inclusion of an agent in this architecture is very useful, as it allows to add an new machine to the system very easily: simply by installing the agent program on the new monitored machine. However, this would make an automatic deployment of the whole system more difficult, as it would require the deployment process to be able to install the agents on each target remotely, i.e. require administrative rights on all the target machines.

> As a consequence, to simplify your future deployment phase, we will simplify this schema a little bit, an use an "agentless" architecture: instead of installing an agent on every monitored machine to collect and send data to a centralized database, we will consider that there's a unique "agent" on this same centralized machine that will connect to each monitored machine through ssh connexions to collect each piece of data.

Your final solution will therefore have an architecture that look more like this:

<img alt="Architecture of your solution" src="./imgs/wanted_architecture.png" width="45%"/>

[^turnkey] "Clef en main".

## Outline of the Labs (and links to the detailed pages)

- [**Introduction to git branching**](./00_Intro_Git_Branching.md)
- [**Lab 1 :** Write scripts to get CPU/RAM info from remote machines](./01_Scripts1.md)
- [**Lab 2 :** Activate & write tests for above code](02_Tests1.md)
- [**Lab 3 :** Write scripts to extract info from LOGs on remote machines (with RegExes)](03_Scripts2.md)
- [**Lab 4 :** Write tests for LOG part](04_Tests2.md)
- [**Lab 5 :** Lint / Coverage](05_LintCoverage.md)
- [**Lab 6&7 :** Framework Web + Admin pour tous *2](06_WebUI.md)
- [**Lab 8&9 :** Docker *2](07_Docker.md)
- [**Lab 10&11 :** Delivery *2](08_Delivery.md)
- **Lab 12&13 :** Finalization of the project
- [**Lab 14 :** Defense](09_Defense.md)

[**To be continued...**](10_IWantDevOpsInMyFutureJob.md)

## Virtual Machine

Many of tools we use in this project (Python, Git, Docker...) are easier to install and work better under GNU/Linux. We have prepared a very light VirtualBox Virtual Machine with all the required tools. It's not mandatory to use it, but you are highly advised to do so :) ([See Docker Lab](07_Docker.md#preliminary-exercises) for more details)

## Resources

Here are some additional resources to help you during this course:

- [DZone's "Continuous Integration Patterns & AntiPatterns"](https://dzone.com/storage/assets/11565134-dzone-refcard84-cipatternsandantipatterns.pdf)
- [DZone's "Continuous Delivery Patterns & AntiPatterns"](https://dzone.com/storage/assets/10862832-dzone-refcard145-cdpatternsandantipatterns.pdf)
- [DZone's "Continuous Integration Servers & Tools"](https://dzone.com/storage/assets/4055325-dzone-refcard087-ci-serversandtools.pdf)
- [DZone's "Continuous Integration/Delivery with Containers"](https://dzone.com/storage/assets/10892980-dzone-refcard276-cicdwithcontainers.pdf)
- [ELK-Stack](https://www.elastic.co/what-is/elk-stack)
