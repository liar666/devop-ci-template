# Continuous delivery

## Use Docker for a better delivery

As we saw in the last section, Docker allows to share container images with all the dependencies of a projet pre-installed. At this point, we use this possibility to run the pipeline more quickly with a custom image for Gitlab CI. But we can go beyond this...

As our custom image is able to run all the tests of the project, that means it can run the project itself. To make you project easier to use, you can create a docker image with:

- All the librairies needed to run the project (as you did for the Gitlab CI image)
- The source code itself
- An entrypoint pointing to your program

The entrypoint of a Docker container is the program run by the container at startup. If that program stops, the container will stop too.

This way, a user can `docker run` your image to get your program running in a container without installing anything on their machine! The only thing missing there is to let the user access the port 8080 (or the one used by the framework you chose) on the container to access the web server.

As a DevOps team, you obviously need to automate this step in your CI.

Eventually, you will be able to run your project with

```bash
$ docker run index.docker.io/you-team-name/printerfadm/image_name
```

and access the right port on your browser (the `-p` option makes the port provided as its argument accessible from outside)

## Building and pushing docker images from Gitlab CI

To push an image to the Gitlab registry we need... Docker. Obviously. That means the Gitlab CI image used to run that job must have docker installed. Yet, this is the only job that needs docker, and it doesn't need the project dependencies. For this particular job, we will use a specific image, at the job level. It is also necessary to ensure the docker service is running with the `services` key.

Docker needs environnement variables to run properly. The 3 commands to publish your docker image:

- `docker login <registery_url>`
- `docker build <build options>`
- `docker push <image name>`

All this combined, you have a fully functional job that builds your image with the source code and its dependencies and publishes it on Gitlab Registry!

```yaml
image: docker:19.03.12

variables:
  CI_REGISTRY_USER: <dockerio-username>
  CI_REGISTRY_PASSWORD: <dockerio-password>
  CI_REGISTRY: docker.io
  CI_REGISTRY_IMAGE: index.docker.io/$CI_REGISTRY_USER/<imagename>
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""

services:
  - name: docker:dind
    entrypoint: ["env", "-u", "DOCKER_HOST"]
    command: ["dockerd-entrypoint.sh"]
    

before_script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY

build:
  stage: build
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE" .
    - docker push "$CI_REGISTRY_IMAGE"
```

[//]: # (build:
  image: docker:19.03.1
  stage: deploy
  services:
    - docker:19.03.1-dind
  variables:
    # Use TLS https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: "/certs"
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"
    - docker build -t "$IMAGE_TAG" .
    - docker push "$IMAGE_TAG")

## Go further...

With the previous configuration, each `docker push` command will override the previous build... That means a commit pushed on a feature branch will eventually replace the latest master image by a installable one. To prevent that, use the `only` key in the deploy job to ensure that the deployment only occurs for the `master` branche.

## TODO

- Create a Dockerfile installing all the dependencies of your project (= what you did in the Gitlab CI container, but with Dockerfile)
- Use a `COPY` statement in the Dockerfile to copy your source code into the container
- Open the web server port with the `EXPOSE` statement in the Dockerfile
- Set the entrypoint to your python program with `ENTRYPOINT`
- Add a new pipeline stage `deploy` that builds and pushes the new image on the Docker Registry
- Activate the `deploy` stage only for the `develop` branche

## Resources

+ [Documentation about Docker and Gitlab](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html)
+ [Documentation about creating a Docker image in your pipeline](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#using-docker-in-docker-image-from-container-registry)
+ [Documentation about using Gitlab variables for naming your image](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#container-registry-examples) [Lien2](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
+ [Run Dash in a Docker container & make it accessible from outside](https://community.plotly.com/t/running-dash-app-in-docker-container/16067/4) [link2](https://digicactus.com/conteneuriser-vos-applications-python-part-1/) [link3](https://www.docker.com/blog/containerized-python-development-part-1/)
